const nodemailer = require('nodemailer');
require("dotenv").config();

const sendCreateOrder = async (recipientEmail) => {
    const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            user: process.env.EMAIL_USERNAME,
            pass: process.env.EMAIL_PASSWORD,
        },
    });
    const orderLink = `http://localhost:3000/user/orders`; 

    const emailContent = `
        Bạn đã đặt hàng thành công. Xem chi tiết đơn hàng tại đây: ${orderLink}
    `;

    const info = await transporter.sendMail({
        from: "L-N Funiture <info@lnfurniture.com>",
        to: recipientEmail,
        subject: "Đặt hàng thành công",
        text: emailContent,
    });
}

module.exports = {
    sendCreateOrder
}
