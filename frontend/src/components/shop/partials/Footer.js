// import React, { Fragment } from "react";
import moment from "moment";

// const Footer = (props) => {
//   return (
//     <Fragment>
//       <footer
//         style={{ background: "#303031", color: "#87898A" }}
//         className="z-10 py-6 px-4 md:px-12 text-center"
//       >
//         Develop & Design Mr. 
//         © Copyright {moment().format("YYYY")}
//       </footer>
//     </Fragment>
//   );
// };

// export default Footer;
import React from "react";

const Footer = () => {
  return (
    <footer className="bg-gray-500 py-8">
      <div className="container mx-auto grid grid-cols-1 md:grid-cols-3 gap-8">
        <div className="text-center md:text-left">
          <h3 className="text-2xl font-bold mb-4">L - N Furniture</h3>
          <p className="text-sm mb-2">Địa chỉ: SPKT</p>
          <p className="text-sm">Email: info@lnfurniture.com</p>
          <p className="text-sm mt-2">Hotline: 0868225520</p>
        </div>
        <div className="hidden md:flex justify-center">
          {/* Biểu tượng mạng xã hội có thể được thêm vào đây nếu cần */}
        </div>
        <div className="text-center md:text-right">
          <p className="text-sm">© Copyright {moment().format("YYYY")}</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;

