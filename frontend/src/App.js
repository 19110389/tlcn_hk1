import React, { Fragment, useReducer } from "react";
import Routes from "./components";
import { LayoutContext, layoutState, layoutReducer } from "./components/shop";
//import Chat from "./components/shop/chatbot/Chat";
import { useEffect } from "react";

function App() {
  const [data, dispatch] = useReducer(layoutReducer, layoutState);
  useEffect(() => {
    document.title = "L-N Furniture";
    return () => {
      document.title = "React App";
    };
  }, []);
  return (
    <div>
      {/* <Chat /> */}
      <Fragment>
        <LayoutContext.Provider value={{ data, dispatch }}>
          <Routes />
        </LayoutContext.Provider>
      </Fragment>
    </div>
  );
}

export default App;
